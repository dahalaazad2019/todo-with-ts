import React, {useState} from 'react';
import './App.css';
import InputField from "./components/InputField";
import {Todo} from "./components/model";
import Todolist from "./components/TodoList";
import {DragDropContext, DropResult} from "react-beautiful-dnd";

const App: React.FC = () => {
    const [todo, setTodo] = useState<string>('');
    const [allTodos, setAllTodos] = useState<Todo[]>([]);
    const [completedTodos, setCompletedTodos] = useState<Todo[]>([]);
    //console.log(allTodos)

    const handleAdd = (event: React.FormEvent) => {
        event.preventDefault();
        if (todo) {
            setAllTodos([...allTodos, {id: Date.now(), todo: todo, isDone: false}]);
            setTodo('');
        }
        console.log('add clicked')
    }

    const onDragEnd = (result: DropResult) => {
        //console.log(result)
        const {source, destination} = result;
        if (!destination) {
            return;
        }
        ;
        if (destination.droppableId === source.droppableId &&
            destination.index === source.index) {
            return
        }
        ;
        let add;
        let active = allTodos;
        let complete = completedTodos;

        //Source Logic
        if (source.droppableId === 'TodosList') {
            add = active[source.index];
            active.splice(source.index, 1)

        } else {
            add = complete[source.index];
            complete.splice(source.index, 1)
        }

        //Destination Logic
        if (destination.droppableId === 'TodosList') {
            active.splice(destination.index, 0, add);

        } else {
            complete.splice(destination.index, 0, add);
        }

        setCompletedTodos(complete);
        setAllTodos(active);

    }
    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <div className="App">
                <span className="heading">Taskify</span>
                <InputField todo={todo} setTodo={setTodo} handleAdd={handleAdd}/>
                <Todolist
                    allTodos={allTodos}
                    setAllTodos={setAllTodos}
                    completedTodos={completedTodos}
                    setCompletedTodos={setCompletedTodos}
                />
            </div>
        </DragDropContext>
    );
}

export default App;
