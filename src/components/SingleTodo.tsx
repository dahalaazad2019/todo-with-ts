import {Todo} from "./model";
import {AiFillDelete, AiFillEdit} from "react-icons/ai";
import {MdDone} from "react-icons/md";
import './styles.css';
import TodoList from "./TodoList";
import {useEffect, useRef, useState} from "react";
import {Simulate} from "react-dom/test-utils";
import input = Simulate.input;
import {Draggable} from "react-beautiful-dnd";

interface Props {
    index: number;
    todo: Todo;
    allTodos: Todo[];
    setAllTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
}

const SingleTodo: React.FC<Props> = ({index, todo, allTodos, setAllTodos}) => {
    const [edit, setEdit] = useState<boolean>(false);
    const [editTodo, setEditTodo] = useState<string>(todo.todo);
    const inputRef = useRef<HTMLInputElement>(null);
    useEffect(() => {
        inputRef.current?.focus();
        ;
    }, [edit]);


    const handleDone = (id: number) => {
        setAllTodos(allTodos.map((todo) => todo.id === id
            ? {...todo, isDone: !todo.isDone}
            : todo))
    }

    const handleDelete = (id: number) => {
        setAllTodos(allTodos.filter((todo) => todo.id !== id))
    }

    const handleEdit = (event: React.FormEvent, id: number) => {
        event.preventDefault();
        setAllTodos(allTodos.map((todo) => (
            todo.id === id ? {...todo, todo: editTodo} : todo)));
        setEdit(false);
    }
    return (
        <Draggable draggableId={todo.id.toString()} index={index}>
            {(provided,snapshot) => (
                <form
                    className={`todos__single ${snapshot.isDragging ? 'drag' : ''}`}

                    onSubmit={(event) => handleEdit(event, todo.id)}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    ref={provided.innerRef}
                >
                    {
                        edit ? (
                            <input value={editTodo}
                                   ref={inputRef}
                                   className='todos__single--text'
                                   onChange={(event) => setEditTodo(event.target.value)}/>
                        ) : !todo.isDone ? (
                            <span className="todos__single--text">{todo.todo}</span>
                        ) : (
                            <s className="todos__single--text">{todo.todo}</s>
                        )
                    }

                    <div>
                <span className="icon" onClick={() => {
                    if (!edit && !todo.isDone) {
                        setEdit(!edit)
                    }
                }
                }>
                    <AiFillEdit/>
                </span>
                        <span className="icon" onClick={() => handleDelete(todo.id)}>
                    <AiFillDelete/>
                </span>
                        <span className="icon" onClick={() => handleDone(todo.id)}>
                    <MdDone/>
                </span>
                    </div>

                </form>
            )

            }
        </Draggable>
    );
};

export default SingleTodo;
