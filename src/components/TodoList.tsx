import {Todo} from "./model";
import SingleTodo from "./SingleTodo";
import {Droppable} from "react-beautiful-dnd";

interface Props {
    allTodos: Todo[];
    setAllTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
    completedTodos: Todo[];
    setCompletedTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
}


const Todolist: React.FC<Props> = ({allTodos, setAllTodos, completedTodos, setCompletedTodos}) => {
    //console.log(allTodos)
    console.log(completedTodos)
    return (
        <div className="container">
            <Droppable droppableId='TodosList'>
                {(provided,snapshot) => (
                    <div className={`todos ${snapshot.isDraggingOver ? 'dragActive' : ''}`}
                         ref={provided.innerRef}
                         {...provided.droppableProps}
                    >
                        <span className="todos__heading">ActiveTasks</span>
                        {
                            allTodos?.map((todo, index) => (
                                <SingleTodo
                                    index={index}
                                    todo={todo}
                                    allTodos={allTodos}
                                    setAllTodos={setAllTodos}
                                    key={todo.id}/>
                            ))
                        }
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>

            <Droppable droppableId='TodosRemove'>
                {(provided,snapshot) => (
                    <div className={`todos ${snapshot.isDraggingOver ? 'dragComplete' : ''}`}
                         ref={provided.innerRef}
                         {...provided.droppableProps}
                    >
                        <span className="todos__heading">Completed Tasks</span>

                        {
                            completedTodos?.map((todo, index) => (
                                <SingleTodo
                                    index={index}
                                    todo={todo}
                                    allTodos={completedTodos}
                                    setAllTodos={setCompletedTodos}
                                    key={todo.id}/>
                            ))
                        }
                        {provided.placeholder}
                    </div>
                )

                }
            </Droppable>
        </div>

    );
};

export default Todolist;
